#!/bin/bash

files=($(ls ./*.mp4))
for line in ${files[@]}
do
	ffmpeg -i $line -vcodec png -r 0.4 ./input/image_%03d.png
done


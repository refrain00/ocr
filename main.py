import sys

import pyocr
import pyocr.builders

import Image_process_modules as ipm


# prepare to use tesseract
tools = pyocr.get_available_tools()
if len(tools) == 0:
    print("No OCR tool found")
    sys.exit(1)

tool = tools[0]
print("Will use tool '%s'" % (tool.get_name()))

langs = tool.get_available_languages()
print("Available languages: %s" % ", ".join(langs))

# get the name of video
output_name = ipm.refer_videos("./")[0].split(".")[1]

# get paths of images
image_paths = ipm.refer_images("./input/")

with open("./output/" + output_name + ".txt", "x") as f:
    for image_path in image_paths:
        text = tool.image_to_string(ipm.preprocess_commu_text(image_path), lang="jpn", builder=pyocr.builders.TextBuilder())
        f.write(text)
        f.write("\n\n")

print("Completed Text Recognition!!!")
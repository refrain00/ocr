FROM nvidia/cuda:9.2-cudnn7-devel-ubuntu18.04
LABEL maintainer=refrain

ENV USER_NAME hogehoge
ENV UID hogehoge
RUN useradd -m -u ${UID} ${USER_NAME} 
RUN gpasswd -a ${USER_NAME} sudo
ENV HOME /Users/${USER_NAME}
WORKDIR ${HOME}

RUN apt-get update \
 && apt-get install -y git \
    curl \
    vim \
    build-essential \
    python3-pip \
    locales \
    libgl1-mesa-dev \
    fontconfig \
    fonts-ipaexfont \
    tesseract-ocr \
    libtesseract-dev \
    tesseract-ocr-jpn \
    tesseract-ocr-script-jpan \
 && locale-gen ja_JP.UTF-8 \
 && pip3 install --upgrade pip
RUN pip3 install numpy \
 && pip3 install tensorflow-gpu==2.3.0 \
 && pip3 install ruamel_yaml \
 && pip3 install --upgrade matplotlib \
 && pip3 install python-dateutil \
 && pip3 install pyocr \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

ENV PYTHONIOENCODING utf-8
ENV LANG ja_JP.UTF-8

RUN chown -R ${USER_NAME}: ${HOME}/.cache/

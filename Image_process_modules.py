import glob
import json
from PIL import Image, ImageFilter


"""
Support you to process images by Pillow.
"""

def refer_images(directory_path):
    """
    return file paths in the directory
    directory_path: string. directory path
    """

    file_paths = glob.glob(directory_path + "*.png")
    file_paths += glob.glob(directory_path + "*.PNG")
    file_paths += glob.glob(directory_path + "*.jpg")
    file_paths += glob.glob(directory_path + "*.jpeg")
    file_paths.sort()

    return file_paths

def refer_videos(directory_path):
    file_paths = glob.glob(directory_path + "*.mp4")

    return file_paths

def read_json(file_path):
    """
    transfer json data to dictionary data and return it.
    file_path: string. json file path
    """
    json_data = json.load(open(file_path))
    return json_data

def read_image(file_path):
    """
    read image by Pillow
    """
    return Image.open(file_path)

def get_PixelValue(image, x_position, y_position):
    """
    return tuple of pixel values of a position specified in a image.
    file_path: string. target image file path
    position: list. [x_position, y_position]
    """
    return image.getpixel((x_position, y_position))

def convert_grayscale(image):
    """
    convert to grayscale
    """
    return image.convert("L")

def convert_binary(image):
    """
    convert to binary
    """
    return convert_grayscale(image).point(lambda x: 0 if x < 132 else x)

def crop(image, left, upper, right, lower):
    """
    crop by Pillow
    """
    return image.crop((left, upper, right, lower))

def save_image(image, file_path):
    image.save(file_path)

def preprocess_commu_text(file_path):
    """
    preprocess for mirishita commu text
    """
    return convert_binary(crop(read_image(file_path), 580, 850, 1710, 990))


if __name__ == "__main__":
    image_path = refer_files("./input/")[7]
    #img = read_image(image_path)
    #cropped_img = crop(img, 580, 800, 1770, 1000)
    save_image(preprocess_image(image_path), "./" + image_path.split("/")[-1])
    

